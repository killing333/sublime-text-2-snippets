#Sublime Text 2 Snippets
## HTML
### imgpl - Image Placeholder
    <img src="http://placehold.it/${1:width}x${2:height}">

### none - Inline Style: Display None
    style="display: none;"

## Javascript
### funcde - Default Value for Function
    ${1:variable} = typeof ${1:variable} !== 'undefined' ? ${1:variable} : ${2:default};

### undef - Check if certain variable is typeof undefined
    typeof ${1:val} !== 'undefined'

## jQuery
### ean - Each jQuery Node do
    each(function( index ) {
        ${1}
    });

### eao - Each Object do
    $.each(${1:array}, function( index, ${2:object in array} ) {
        ${3}
    });

### eak - Each Key in Object do
    $.each(${1:object}, function( key, value ) {
        ${2}
    });

### gjson - $.getJSON 
    $.getJSON( ${1:URL}, {
        ${2:param}: ${3:value}
    })
    .done(function(response){
        ${4:Callback when responsed}
    });

### js - Document.ready Template
    // Initialization or settings
    var init = function () {
    };
    
    // Bind all events to nodes
    var bindEvents = function () {
    };
    
    $(function() {
        init();
        bindEvents();    
    });

### new - Create new node
> In jQuery, typing `$("<foo>")` would create a new node of "foo", ie. `<foo></foo>`
~~~~
$("<${1:name}>")
~~~~

## ALL
### cmt - Comment Block
    /*--------------------------------------------------------------------------------------------------
     |
     |    ${1:Comment}
     |
     *-------------------------------------------------------------------------------------------------*/
